package experiments;

/******************************************************************************
 * This experiment demonstrates the use of access modifiers in Java. They are
 * Java's way of implementing data hiding and restricting which methods are
 * accessible outside the class.
 * 
 * If you were to remove the comments this code would try to access the private
 * members squareB and b. The compiler will complain about this.
 * 
 * @author Dr Kevan Buckley, University of Wolverhampton, 2017
 ******************************************************************************/

public class Reflection02 {
  public static void main(String[] args) {
	SimpleTestExperiment  a = new SimpleTestExperiment ();
	a.squareNum1();
    // a.squareNum2(); // if you uncomment this you will get a compiler error
    double num1 = a.num1;
    // num2 = a.num2; // if you uncomment this you will get a compiler error
    System.out.println("Accessing Private Members of SimpleTestExperiment =" + a);
  }
}
